//
//  txtFileIO.m
//  CocoaConsole
//
//  Created by zhongdian on 15/3/19.
//  Copyright (c) 2015年 zhongdian. All rights reserved.
//

#import <Foundation/Foundation.h>

int fileIO() {
    @autoreleasepool {
        
        NSLog(@"Put a maiku.txt file on your Desktop and input a num");
        
        //目前还没有发现像.NET那么方面的ReadKey方法
        int readKey;
        scanf("%d",&readKey);
        
        //处理用数据
        NSArray *rawTxt;
        NSString *outString = @"Result:";
        
        //访问用数据
        NSString *inPath = @"/Users/zhongdian/Desktop/maiku.txt";
        NSString *outPath = @"/Users/zhongdian/Desktop/maiku_out.txt";
        NSError *error;
        
        //输入文件
        rawTxt = [[NSString stringWithContentsOfFile:inPath encoding:NSUTF8StringEncoding error:&error] componentsSeparatedByString:@"\n"];
        
        //输入转输出
        int count=0;
        while (count < rawTxt.count) {
            if (![rawTxt[count]  isEqual: @""]) {
                outString = [NSString stringWithFormat:@"%@%@%@",outString,@"\n",rawTxt[count]];
            }
            count++;
        }
        
        //创建文件，这里的contents就可以直接写文件了吗？
        NSFileManager *FM = [NSFileManager defaultManager];
        [FM createFileAtPath:outPath contents:nil attributes:nil];
        
        //写入输出
        NSFileHandle *FH = [NSFileHandle fileHandleForWritingAtPath:outPath];
        NSData *outData = [outString dataUsingEncoding:NSUTF8StringEncoding];
        [FH writeData:outData];
        [FH closeFile];
        
        // If success, Program will end with exit code: 0
        NSLog(@"------That's All-------");
    }
    return 0;
}
